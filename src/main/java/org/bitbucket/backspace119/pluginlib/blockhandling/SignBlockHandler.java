package org.bitbucket.backspace119.pluginlib.blockhandling;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
/**
 * This class is used for handling events dealing with signs. Just extend this class and implement its abstract methods and then register it as an event handler
 * in your plugin. Most of the methods are straightforward and require little to no documentation, but docs are available if you require help.
 * @author backspace119
 *
 */
public abstract class SignBlockHandler implements Listener{

	private final String signFirstLine;
	private final ChatColor firstLineColor;
	public SignBlockHandler(String signFirstLine, ChatColor firstLineColor)
	{
		this.signFirstLine = signFirstLine;
		this.firstLineColor = firstLineColor;
	}
	
	/*
	 * this most likely never works....consideration should be taken for removing it
	 */
	@EventHandler
	public void signPlaceEvent(BlockPlaceEvent event)
	{
		
		if(event.getBlock().getType().name().contains("SIGN"))
		{
			
			Sign sign = (Sign) event.getBlock().getState();
			if(sign.getLine(0).equalsIgnoreCase(signFirstLine))
			{
				sign.setLine(0, firstLineColor + signFirstLine);
				onSignPlace(sign,event.getPlayer());
			}
		}
	}
	
	@EventHandler
	public void signChangeEvent(SignChangeEvent event)
	{
		if(event.getLine(0).equalsIgnoreCase(firstLineColor + signFirstLine) || event.getLine(0).equalsIgnoreCase(signFirstLine))
		{
			
			if(event.getLine(0).equalsIgnoreCase(signFirstLine))
			{
				event.setLine(0, firstLineColor + signFirstLine);
				
			}
			onSignChange((Sign) event.getBlock().getState(),event.getPlayer(),event.getLines(),event);
		}
	}
	
	@EventHandler(ignoreCancelled=true)
	public void signClickEvent(PlayerInteractEvent event)
	{
		
		//System.out.println("click event");
		if(event.getClickedBlock() != null)
		{
			if(event.getClickedBlock().getType().name().contains("SIGN"))
			{
				event.setCancelled(false);
				Sign sign = (Sign) event.getClickedBlock().getState();
				
				if(sign.getLine(0).equals(firstLineColor + signFirstLine))
				{
				if(event.getAction().equals(Action.LEFT_CLICK_BLOCK))
				{
					onSignLeftClick(sign,event.getPlayer());
				}else if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
				{
					onSignRightClick(sign,event.getPlayer());
				}
				
				}
			}
		}
	}
	/**
	 * Thrown when the sign is placed. RARELY WORKS! WILL BE DEPRECATED IN NEXT VERSION MOST LIKELY!
	 * @param sign the sign that was placed
	 * @param player the player that placed the sign
	 */
	public abstract void onSignPlace(Sign sign,Player player);
	/**
	 * Thrown when a player right clicks on a sign that contains the first line you submitted in the constructor. 
	 * @param sign the sign that was clicked
	 * @param player the player that clicked it
	 */
	public abstract void onSignRightClick(Sign sign,Player player);
	/**
	 * Thrown when a player left clicks on a sign that contains the first line you submitted in the constructor.
	 * @param sign the sign that was clicked
	 * @param player the player that clicked the sign
	 */
	public abstract void onSignLeftClick(Sign sign,Player player);
	/**
	 * thrown when a sign's lines are changed (this includes when the sign is placed and the lines are initially changed from blank to something that includes
	 * the first line you submitted in the constructor and the other lines).
	 * @param sign the sign that was changed (lines tend to be blank when called from this, lines are provided because of this bug)
	 * @param player the player that changed the lines
	 * @param lines the lines of the sign (lines tend to be blank when retrieving from the sign object in this so these are provided in their stead)
	 */
	public abstract void onSignChange(Sign sign,Player player,String[] lines,SignChangeEvent event);
	
}
