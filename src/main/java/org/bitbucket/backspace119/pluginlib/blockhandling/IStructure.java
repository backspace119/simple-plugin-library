package org.bitbucket.backspace119.pluginlib.blockhandling;

import java.util.List;

import org.bukkit.Location;
/**
 * This interface is provided as a skeleton of any generic structure object. There is a default impelementation included 
 * in this package called "Structure" that defines this interface as it was intended to be defined. This should not, however,
 * stop someone else from making an implementation they deem to be better for their, or others, use.
 * @author backspace119
 *
 */
public interface IStructure {

	/**
	 * This returns the structure's origin in relation to the rest of the world (the origin being the "back left" of the structure)
	 * @return the location from the "back left" of the structure. This implies that the structure as a whole resides in quadrant 1 of a Cartesian plane
	 */
	public Location getOrigin();
	
	/**
	 * 
	 * @return a simple cache of all StructureLocations (locations relative to structure origin that are included in the structure)
	 */
	public List<StructureLocation> getStructureLocs();
	
	/**
	 * This is a method to provide easy retrieval of locations from a structure for modifying or personal logic
	 * @param x the x value relative to the structure's origin
	 * @param y the y value relative to the structure's origin
	 * @param z the z value relative to the structure's origin
	 * @return the StructureLocation included in the structure that matches the input coordinates. 
	 */
	public StructureLocation getStructureLocation(int x, int y, int z);
	
	/**
	 * This will rotate the structure left. THIS IS DESTRUCTIVE, IT WILL CHANGE YOUR ISTRUCTURE OBJECT!
	 */
	public void rotateLeft();
	
	/**
	 * This will rotate the structure right. THIS IS DESTRUCTIVE, IT WILL CHANGE YOUR ISTRUCTURE OBJECT!
	 */
	public void rotateRight();
	
	/**
	 * 
	 * @return a clone of this structure, identical except rotated 90 degrees left.
	 */
	public IStructure cloneRotateLeft();
}
