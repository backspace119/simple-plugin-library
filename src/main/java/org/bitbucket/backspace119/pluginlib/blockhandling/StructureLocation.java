package org.bitbucket.backspace119.pluginlib.blockhandling;

import org.bukkit.Material;
/**
 * part of the new structure system introduced in v. 0.14.0-BETA. This is a relative location class that carries data of a block to be built in a structure.
 * This is still highly experimental and great care should be taken when using any of this system.
 * @author backspace119
 *
 */
public class StructureLocation {

	public StructureLocation(int x, int y, int z, Material mat)
	{
		this.mat = mat;
		this.x = x;
		this.y = y;
		this.z = z;
		
	}
	private int x;
	private int y;
	private int z;
	private Material mat;
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public int getZ()
	{
		return z;
	}
	public void setX(int x)
	{
		this.x = x;
	}
	public void setY(int y)
	{
		this.y = y;
	}
	public void setZ(int z)
	{
		this.z = z;
	}
	public Material getMaterial()
	{
		return mat;
	}
	public void setMaterial(Material mat)
	{
		this.mat = mat;
	}
}
