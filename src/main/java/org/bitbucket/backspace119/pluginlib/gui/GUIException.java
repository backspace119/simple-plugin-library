package org.bitbucket.backspace119.pluginlib.gui;

public class GUIException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public GUIException(String message)
	{
		super(message);
	}
	public GUIException(Throwable cause)
	{
		super(cause);
	}
	public GUIException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
