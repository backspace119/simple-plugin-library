package org.bitbucket.backspace119.pluginlib.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class Menu implements IMenu{

	private final ItemStack[] MENU_CONTENTS;
	private final Plugin plugin;
	private final IMenuSelectionHandler selectHandler;
	public Menu(Plugin plugin,ItemStack[] items, IMenuSelectionHandler selectHandler)
	{
		if(items.length > 27)
		{
			throw new GUIException("Menu's use chest inventories, ItemStack arrays must have a length no greater than 27");
		}
		MENU_CONTENTS = items;
		this.plugin = plugin;
		this.selectHandler = selectHandler;
		
	}
	
	public Menu(Plugin plugin,Inventory inv, IMenuSelectionHandler selectHandler)
	{
		if(inv.getType().equals(InventoryType.CHEST))
		{
			throw new GUIException("Menu inventories must be chest inventories");
		}
		
		MENU_CONTENTS = inv.getContents();
		this.plugin = plugin;
		this.selectHandler = selectHandler;
	}
	
	@Override
	public Inventory getMenu(Player p) {
		Inventory inv = plugin.getServer().createInventory(p, InventoryType.CHEST);
		inv.setContents(MENU_CONTENTS);
		return inv;
	}

	@Override
	public ItemStack[] getMenuItems() {
		return MENU_CONTENTS;
	}

	@Override
	public void selectionMade(Player p, int slot) {
		selectHandler.selectionMade(p, slot);
	}

}
