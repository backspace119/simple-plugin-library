/*
 *   Copyright (c) 2019 backspace119
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.bitbucket.backspace119.pluginlib.gui;

import java.util.ArrayList;
import java.util.List;
import org.bitbucket.backspace119.pluginlib.exceptions.MalformedMessageException;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;

/**
 * this class will act as a wrapper for any GUI functions. When it is finished, it will standardize
 * coloration, message format, and general look and feel of gui. Until then it will wrap a few of the
 * normal bukkit functions as well as adding some extra functionality
 *
 * @author backspace119
 */
public class GUIManager {
    
    public GUIManager(Server server) {
        this.server = server;
    }
    
    private final Server server;
    private ChatColor headerColor = ChatColor.GOLD;
    private ChatColor footerColor = ChatColor.GOLD;
    private char headerCharacter = '=';
    private char footerCharacter = '=';
    private ChatColor positive = ChatColor.GREEN;
    private ChatColor negative = ChatColor.RED;
    private ChatColor moderate = ChatColor.BLUE;
    private ChatColor important = ChatColor.BOLD;
    private final List<String> lines = new ArrayList<>();
    
    /**
     * set the character to use in the header of the headedFootedMessage() method
     *
     * @param headerCharacter
     */
    public void setHeaderCharacter(char headerCharacter) {
        this.headerCharacter = headerCharacter;
    }

    /**
     * set the character to use in the footer of the headedFootedMessage() method
     *
     * @param footerCharacter
     */
    public void setFooterCharacter(char footerCharacter) {
        this.footerCharacter = footerCharacter;
    }

    /**
     * set the color of the header that will be displayed by headedFootedMessage()
     *
     * @param headerColor
     */
    public void setHeaderColor(ChatColor headerColor) {
        this.headerColor = headerColor;
    }

    /**
     * set the color of the footer that will be displayed by headedFootedMessage()
     *
     * @param footerColor
     */
    public void setFooterColor(ChatColor footerColor) {
        this.footerColor = footerColor;
    }
    
    private String pos(String m) {
        return positive + m;
    }

    private String neg(String m) {
        return negative + m;
    }

    private String im(String m) {
        return important + m;
    }

    private String mod(String m) {
        return moderate + m;
    }

    /**
     * sets the color to apply to messages marked as Positive
     *
     * @param positive
     */
    public void setPositive(ChatColor positive) {
        this.positive = positive;
    }

    /**
     * sets the chat color to apply to messages that are negative
     *
     * @param negative
     */
    public void setNegative(ChatColor negative) {
        this.negative = negative;
    }

    /**
     * sets the chat color to apply to messages that are neither positive nor negative
     *
     * @param moderate
     */
    public void setModerate(ChatColor moderate) {
        this.moderate = moderate;
    }

    /**
     * sets the format to apply to messages that are marked important.
     * Please note that this is additive to other message types, and therefore
     * should only have format type ChatColor values applied to it, such as
     * ChatColor.BOLD or ChatColor.ITALICS
     *
     * @param important
     */
    public void setImportant(ChatColor important) {
        this.important = important;
    }

    /**
     * use this to format any messages you send to this class. This will help you standardize your
     * message formats for various things.
     * POS is for positive messages (i.e. "YOU WON!")
     * NEG is for negative messages (i.e. "You are not allowed to enter this region!")
     * MOD is for neither positive nor negative messages. (i.e. "Wrong usage of command, use like:") (please note
     * that commands registered to this library will have this formatting applied to their usage strings, no need
     * to apply any formatting to those strings)
     * the _I indicates an important message, one that should stick out to the player so as to get them
     * to read it. Important messages add formatting such as bold or italics to the text, depending
     * on what you've set to setImportant()
     *
     * @author backspace119
     */
    public enum Formatting {
        //need to find a way
        //to store format in this
        //enum
        POS_I,
        NEG_I,
        POS,
        NEG,
        MOD_I,
        MOD,
        DEFAULT;
        
    }

    ChatColor boxColor = ChatColor.DARK_BLUE;
    ChatColor boxFormat = ChatColor.BOLD;
    char boxCharacter = '-';
    char boxWallCharacter = '|';
    ChatColor boxWallColor = ChatColor.DARK_RED;

    /**
     * set the color that boxed message tops and walls will appear as
     *
     * @param boxColor
     */
    public void setBoxColor(ChatColor boxColor) {
        this.boxColor = boxColor;
    }

    /**
     * if you want box walls and tops to be ChatColor.BOLD or ChatColor.ITALICS then you should pass that in here
     * DO NOT pass in colors, they will interfere with the box color
     *
     * @param boxFormat
     */
    public void setBoxFormat(ChatColor boxFormat) {
        this.boxFormat = boxFormat;
    }

    /**
     * set the character to be used in forming the tops and bottoms of boxes
     *
     * @param boxCharacter
     */
    public void setBoxCharacter(char boxCharacter) {
        this.boxCharacter = boxCharacter;
    }

    /**
     * set the character to be used in making the wall that sits beside messages
     * in boxed messages
     *
     * @param boxWallCharacter
     */
    public void setBoxWallCharacter(char boxWallCharacter) {
        this.boxWallCharacter = boxWallCharacter;
    }

    /**
     * used internally to get the string value of a format
     *
     * @param format the Formatting object to use to format a string
     * @return the string character codes that contain the format of the Formatting object
     */
    public String getFormat(Formatting format) {
        switch (format) {
            case DEFAULT:
                return "";
            case MOD:
                return mod("");
            case MOD_I:
                return mod("") + im("");
            case NEG:
                return neg("");
            case NEG_I:
                return neg("") + im("");
            case POS:
                return pos("");
            case POS_I:
                return pos("") + im("");
            default:
                return "";
        }
    }

    /**
     * set the color of
     *
     * @param boxWallColor
     */
    public void setBoxWallColor(ChatColor boxWallColor) {
        this.boxWallColor = boxWallColor;
    }

    /**
     * This is the same as player.sendMessage() EXCEPT that it utilizes the formatting utility in this
     * class. Therefore, it will help keep your formatting between messages consistent.
     *
     * @param p       the CommandSender to send the message to
     * @param message the message to send
     */
    public void simpleMessage(CommandSender p, String message, Formatting format) {
        if (p == null) {

            throw new MalformedMessageException("CommandSender passed to SimpleMessage was null!");
        } else {
            p.sendMessage(getFormat(format) + message);
        }
    }

    /**
     * Sends a message to a CommandSender that has a header and footer
     * your message will appear with this preceding and succeeding it:
     * ================================== (in default configuration)
     * message text will be wrapped normally by bukkit and will appear between the header and footer
     *
     * @param p
     * @param message
     */
    public void headedFootedMessage(CommandSender p, String message, Formatting format) {
        if (p == null) {
            throw new MalformedMessageException("CommandSender passed to HeadedFootedMessage was null!");
        } else {
            p.sendMessage(header());
            p.sendMessage(getFormat(format) + message);
            p.sendMessage(footer());
        }
    }

    /**
     * Broadcast a headed and footed message to the entire server
     *
     * @param message the message to broadcast
     * @param format  the format in which to broadcast it
     */
    public void broadcastHeadedFootedMessage(String message, Formatting format) {
        server.broadcastMessage(header());
        server.broadcastMessage(getFormat(format) + message);
        server.broadcastMessage(footer());
    }

    /**
     * Broadcast a simple message to the entire server
     *
     * @param message the message to broadcast
     * @param format  the format to broadcast it in
     */
    public void broadcastSimpleMessage(String message, Formatting format) {
        server.broadcastMessage(getFormat(format) + message);
    }

    private String header() {
        String header = stringFromChar(headerCharacter);
        return headerColor + header;
    }

    private String footer() {
        String footer = stringFromChar(footerCharacter);
        return footerColor + footer;
    }

    private String stringFromChar(char c) {
        String string = "";
        for (int i = 0; i < 50; i++) {
            string = string + c;
        }
        return string;
    }

    private String box() {
        String box = stringFromChar(boxCharacter);
        return boxForm() + box;
    }


    /**
     * makes a "box" around the message that you want to send to a CommandSender
     * It will be preceded and succeeded by: |-------------------------- (in default configuration) and
     * each line will start with | (in default configuration)
     * the string will be interpreted as a single line message. if you have
     * multiple messages or want to specify lines yourself, use boxedMessageML()
     *
     * @param p       the CommandSender to send the message to
     * @param message the message to send to the CommandSender, wrapped to 45 characters per line
     */
    public void boxedMessageSL(CommandSender p, String message, Formatting format) {
        formatLines(message, true);
        boxedMessage(p, format);
    }

    /**
     * for what this will format to, refer to the doc for boxedMessageSL()
     * this does not wrap! Ever! If you have a string that is more than 45
     * characters long in your array this will throw an exception! This is
     * because you are handling the wrapping here, and if it were to wrap
     * through normal bukkit code it would ruin the "box" effect
     *
     * @param p        CommandSender to send message to
     * @param messages array of messages to send on different lines
     */
    public void boxedMessageML(CommandSender p, String[] messages, Formatting format) throws MalformedMessageException {
        for (String line : messages) {
            if (line.length() > 45) {
                throw new MalformedMessageException("One or more of the lines passed into the String array in boxedMessageML() was greater than 50 characters long. Please shorten this line.");
            }
            lines.add(line);
        }
        boxedMessage(p, format);


    }

    private String boxForm() {
        return boxFormat + "" + boxColor;
    }

    /**
     * call formatLines() or add the lines to lines before calling this
     *
     * @param p
     */
    private void boxedMessage(CommandSender p, Formatting format) {
        //this may throw an exception soon, for the moment,
        //it does not because it is used only in a few places internally
        //where it is certain that lines is setup properly
        if (lines.isEmpty() || lines == null) {
            return;
        }
        if (p != null) {
            p.sendMessage(box());
            for (String line : lines) {
                p.sendMessage(boxForm() + "" + boxWallCharacter + "  " + getFormat(format) + line);
            }
            p.sendMessage(box());
        } else {
            server.broadcastMessage(box());
            for (String line : lines) {
                server.broadcastMessage(boxForm() + "" + boxWallCharacter + "  " + getFormat(format) + line);
            }
            server.broadcastMessage(box());
        }
        lines.clear();
    }

    

    /**
     * used internally to format lines for single message lines
     *
     * @param message    message to format into acceptable lines
     * @param clearLines this should always be true, the reason being that this is a recursive
     *                   function and when it calls itself it sets this to false, to retain data formatted
     *                   by the last run through
     * @return the lines that have been formatted and are ready to be sent. This value is not used and may be removed in later versions.
     */
    private List<String> formatLines(String message, boolean clearLines) {
        //clear lines should be true whenever you call this function
        if (clearLines) {
            lines.clear();
        }
        if (message.length() < 46) {
            lines.add(message);
            return lines;
        }
        String nextLine = "";
        char[] line = message.toCharArray();
        for (int i = 45; i < line.length; i++) {
            nextLine = nextLine + line[i];
            line[i] = ' ';

        }
        String sLine = String.valueOf(line).trim();
        lines.add(sLine);

        if (nextLine.length() > 45) {
            return formatLines(nextLine, false);
        }
        lines.add(nextLine);
        return lines;
    }
}
