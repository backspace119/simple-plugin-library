package org.bitbucket.backspace119.pluginlib.gui;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public interface IPagedMenu extends IMenu{

	public int currentPage();
	public int setPage();
	public void nextPage(Player p);
	public void prevPage(Player p);
	public Inventory getPageInventory(int page);
	public ItemStack[] getPageInventoryContents(int page);
	
}
