package org.bitbucket.backspace119.pluginlib.gui;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public interface IMenu {

	public Inventory getMenu(Player p);
	public ItemStack[] getMenuItems();
	public void selectionMade(Player p, int slot);
	
}
