package org.bitbucket.backspace119.pluginlib.gui;

import org.bukkit.entity.Player;

public interface IMenuSelectionHandler {

	public void selectionMade(Player p, int slot);
}
