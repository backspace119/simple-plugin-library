/*
 *   Copyright (c) 2014 backspace119
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.bitbucket.backspace119.pluginlib.help;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bitbucket.backspace119.pluginlib.commands.CommandHandler;
import org.bitbucket.backspace119.pluginlib.gui.GUIManager;
import org.bukkit.command.CommandSender;
/**
 * this will manage help for your plugin, Although you will still need to write up /a few/ things
 * for the help, this will help automate much of the process of showing player pertinent help data
 * @author backspace119
 *
 */
public class HelpManager {

	Map<String,String> usageMap = new HashMap<>();
	GUIManager guiManager;
	public HelpManager(CommandHandler commandHandler)
	{
		this.guiManager = commandHandler.getGUIManager();
		for(String key: commandHandler.getCommandMap().keySet())
		{
			usageMap.put(key, commandHandler.getCommandMap().get(key).usage());
		}
	}
	
	
	public void giveHelp(CommandSender p, String request)
	{
		if(usageMap.get(request) != null)
		{
			guiManager.boxedMessageSL(p, usageMap.get(request), GUIManager.Formatting.MOD);
		}else{
			//load help file here if it exists and print it to the user.
			guiManager.boxedMessageSL(p, "This plugin was made using the Simple Plugin Library by backspace119. Type /<plugin-base-command> help <command> to get the usage of a command. Type /<plugin-base-command> help list  to get a list of commands", GUIManager.Formatting.MOD);
		}
	}
	public void listCommands(CommandSender sender)
	{
		List<String> messages = new ArrayList<>();
		messages.add(":::List of commands:::");
		messages.addAll(usageMap.keySet());
		String[] m = new String[messages.size()];
		int i = 0;
		for(String message: messages)
		{
			m[i] = message;
			i++;
		}
		guiManager.boxedMessageML(sender, m, GUIManager.Formatting.MOD);
	}
}
