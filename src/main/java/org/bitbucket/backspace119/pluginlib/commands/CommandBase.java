/*
 *   Copyright (c) 2019 backspace119
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.bitbucket.backspace119.pluginlib.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
/**
 * This is the base class for all commands formed by the plugin. Extend this class with a custom command
 * class named of your choosing and implement the methods correctly.
 * 
 * If you wanted a command that would say "hello!" to the player then you could make class Hello extends PluginCommand
 * 
 * return to getName() the name of the command, in our case we want to make the command /<Plugin-Abbreviation> hello
 * so we will: {@code return "hello"; }
 * fill parseCommand() with whatever parameter checking you need to do for this command (in our case we don't have any paramaters so we can leave it blank)
 * MAKE SURE TO RETURN TRUE IF LEFT BLANK OR ELSE IT WILL STOP THE COMMAND FROM EXECUTING! the normal use for this is
 * to return false if the command is malformed, at which case the string from {@code usage()} will be printed
 * then return to {@code targetSender()} the kind of sender you want to accept, whether it be {@code TargetSender.PLAYER, TargetSender.CONSOLE, or TargetSender.BOTH}
 * in our case we want to do TargetSender.PLAYER
 * return to {@code usage()} how the command should be used, in our case we would: {@code return "use like /<Plugin-Abbreviation> hello"}
 * return to {@code permission()} what permission they'll need, in our case {@code return "plugin.hello";}
 * now you're finally ready to fill out the {@code execute()} method. Since we know that it will only be run if a
 * Player sender sends the command (this applies to parseCommand() too) we can go ahead and cast the CommandSender
 * to a player {@code Player player = (Player) sender} and then send a message to said player by: {@code player.sendMessage(ChatColor.GREEN + "hello!");}
 * @author backspace119
 *
 */
public abstract class CommandBase {

	
	/**
	 * this will pass an instance of your plugin into
	 * the command class. You should save the instance so that you may use it later.
	 * @param plugin
	 */
	public abstract void setPlugin(JavaPlugin plugin);

	/**
	 * put all code that should check the args and command setup for correctness in usage. You can optionally use
	 * this to setup everything for the command to run (this is recommended as this is what the method is intended for)
	 * and then allow execute to simply do the actual execution of the command, using the variables and such setup by
	 * this method 
	 * note: for those of you used to having your command be the first arg, it has been stripped. args[0] is the first
	 * argument in the command that was sent.
	 * 
	 * @return the status of the parse, if you want to send custom info to the player and have the library back off,
	 * send back CUSTOM_FEEDBACK if they didn't use it correctly, send back WRONG_USAGE, if everything is fine and dandy
	 * send back SUCCESS
	 */
	public abstract CommandHandler.ParseStatus parseCommand(CommandSender sender, String[] args);
	
	/**
	 * this is run when the command has passed formation inspection ({@code parseCommand()}) and is ready to be run. Return false
	 * to this if anything goes wrong when executing the command or if certain requirements for running the command
	 * were not met. Permission checking and command args parsing is not recommended to be done in here because there
	 * are other methods dedicated to that.
	 * note: for those of you used to having your command be the first arg, it has been stripped. args[0] is the first
	 * argument in the command that was sent.
	 * @return
	 */
	public abstract boolean execute(CommandSender sender, String[] args);
	/**
	 * 
	 * @return the command label to be used for this command. i.e. if you have plugin "derp" and command "herp" then
	 * the command executor should be registered for the command "derp" and the PluginCommand class Herp.class should
	 * return "herp" for it's name, that way, the user can type /derp herp to run the command
	 * 
	 */
	public abstract String getName();
	/**
	 * simply make this return the target user that should be able to send this command. 
	 * If only the console should be able to run it, then it should look like this:
	 * return TargetSender.CONSOLE;
	 * @return the intended user who should be able to send this command. If set to TargetSender.CONSOLE, only the console
	 * will be allowed to run the command. If set to TargetSender.PLAYER, only players (with the correct permissions if you
	 * set them) will be allowed to run the command. If set to TargetSender.BOTH, both the console and players will be able
	 * to run the command
	 * @NonNull
	 * 
	 */
	public abstract TargetSender targetSender();
	/**
	 * This is to standardize all permission checks. If no permission is required, or if you want to do
	 * multiple permission checks in parseCommand() simply return ""
	 * @return the string value of the permission needed to run this command
	 * @NonNull
	 */
	public abstract String permission();
	/**
	 * 
	 * @return the malformed usage message for this command. i.e. "use like </derp herp player> to herp the player with the derp plugin"
	 * @NonNull
	 */
	public abstract String usage();
	
		
	
}
