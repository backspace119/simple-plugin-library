/*
 *   Copyright (c) 2014 backspace119
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.bitbucket.backspace119.pluginlib.commands;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bitbucket.backspace119.pluginlib.gui.GUIManager;
import org.bitbucket.backspace119.pluginlib.gui.GUIManager.Formatting;
import org.bitbucket.backspace119.pluginlib.help.HelpManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.reflections.Reflections;

/**
 * This class is to be used as your main command executor. Come up with an
 * abbreviation of your plugin name to use as your base command, for instance,
 * if we had a plugin named DERPHERPAWHATCHAHOOZIT, we could use the base
 * command: /derp. try and make your base command as uniquely identifiable as
 * possible or you will have conflicts with other plugins. Commands will work
 * like this: /<base-command> <commandName> <arg> <arg> <arg> to get started
 * using this API for your plugin, in your onEnable() method, put this:
 * {@code getCommand("BASE-COMMAND").setExecutor( new CommandHandler(getLogger(),new Reflections("YOUR.BASE.PACKAGE.HERE", getClass().getClassLoader())));}
 * This will register your base command with the CommandHandler and the
 * CommandHandler will search your package for any classes that extend
 * org.bitbucket.backspace119.pluginlib.commands.PluginCommand and cache them
 * for later use.
 * 
 * @author backspace119
 *
 */
public class CommandHandler implements CommandExecutor {

	private Map<String, CommandBase> commandMap = new HashMap<>();
	private GUIManager guiManager;
	private HelpManager helpManager;
	private JavaPlugin plugin;
	public CommandHandler(String mainPackage, JavaPlugin plugin, GUIManager guiManager) {
		
		this.guiManager = guiManager;
		this.plugin = plugin;
		Reflections refl = new Reflections(mainPackage, plugin.getClass().getClassLoader());
		Set<Class<? extends CommandBase>> commandSet = refl.getSubTypesOf(CommandBase.class);
		for (Class<? extends CommandBase> T : commandSet) {
			try {
				Constructor<? extends CommandBase> c = T.getConstructor();
				CommandBase cb = c.newInstance();
				cb.setPlugin(plugin);
				String name = cb.getName();
				commandMap.put(name.toLowerCase(), cb);
			} catch (SecurityException | NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e) {
				
				e.printStackTrace();
			}
		}
		helpManager = new HelpManager(this);
	}

	public Map<String, CommandBase> getCommandMap()
	{
		return commandMap;
	}
	public GUIManager getGUIManager()
	{
		return guiManager;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLbl, String[] args) {

		if (args.length > 0) {
			String subCommand = args[0].toLowerCase();
			
			if (commandMap.containsKey(subCommand)) {
				// thanks to mcmonkey for some pointers on the previous use of reflection here
				CommandBase command = commandMap.get(subCommand);
				TargetSender ts = command.targetSender();
				// this is long winded and not the desired effect of the enum
				// the enum stores the classes so it should be able to check the
				// instance by
				// those somehow
				// TODO fix this
				//could probably fix by sender.getClass().equals(ts.getClass()) or w/e 
				if ((ts.equals(TargetSender.PLAYER) && sender instanceof Player) || (ts.equals(TargetSender.CONSOLE) && sender instanceof ConsoleCommandSender) || ts.equals(TargetSender.BOTH)) {
					String perm = command.permission();
					if (sender.hasPermission(perm) || perm.isEmpty()) {
						String[] commandArgs = new String[args.length - 1];
						System.arraycopy(args, 1, commandArgs, 0, args.length - 1);
						
						switch (command.parseCommand(sender, commandArgs))
						{
						case CUSTOM_FEEDBACK:
							return false;
						case WRONG_USAGE:
							guiManager.boxedMessageSL(sender, command.usage(), Formatting.MOD);
							return false;
						case SUCCESS:
							return command.execute(sender, commandArgs);
						default:
							return false;
						}

					}else{
						guiManager.simpleMessage(sender, "You do not have permission to send that command.", Formatting.NEG);
					}
				} else {
					guiManager.simpleMessage(sender,"You are not allowed to send this command because it is designed for " + ((sender instanceof Player) ? "console ":"player ") + " use only.",Formatting.NEG_I);
					return false;
				}

			}else if(subCommand.equals("help"))
			{
				boolean list = true;
				String help = "";
				if(args.length > 1)
				{
					help = args[1];

					if(args[1].equalsIgnoreCase("list"))
					{
						list = true;
					}
					
				}
				if(list)
				{
				 helpManager.listCommands(sender);
				}else{
				helpManager.giveHelp(sender, help);
				}
			}else{
				guiManager.simpleMessage(sender, "No such command.", GUIManager.Formatting.NEG);
			}
		}else{
			guiManager.simpleMessage(sender, plugin.getName() + " v." + plugin.getDescription().getVersion()  + " authors: " + plugin.getDescription().getAuthors().toString().replaceAll("\\[", "").replaceAll("]",""), GUIManager.Formatting.MOD);
		}
		return false;
	}

	public enum ParseStatus {

		WRONG_USAGE,
		CUSTOM_FEEDBACK,
		SUCCESS;
	}

}