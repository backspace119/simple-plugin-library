/*
 *   Copyright (c) 2014 backspace119
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.bitbucket.backspace119.pluginlib.taskmanagement;


public abstract class PluginTask implements Runnable{

	TaskManager manager;
	public PluginTask(TaskManager manager)
	{
		this.manager = manager;
	}

	
	@Override
	public void run() {
		if(shouldStopExecution())
		{
			postExecute();
			manager.cancelTask(this);
		}else{
			
		execute();
		}
	}
	/**
	 * if this is an endlessly repeating timed task, use this to cancel the task should it be necessary to,
	 * otherwise just return false to this eternally.
	 * @return true if execution of a repeating task should be halted.
	 */
	public abstract boolean shouldStopExecution();
	/**
	 * runs every iteration that the task is run. (if it is a timed task it runs every time the task is run, 
	 * if it is not, it runs once at execution)
	 */
	public abstract void execute();
	/**
	 * runs once before the task dies if shouldStopExecution returns true
	 */
	public abstract void postExecute();
	

}
