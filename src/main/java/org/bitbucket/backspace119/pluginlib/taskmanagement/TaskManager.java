/*
 *   Copyright (c) 2014 backspace119
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.bitbucket.backspace119.pluginlib.taskmanagement;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;
/**
 * This class manages bukkit "tasks" (threads and repeating execution threads.) Instantiate it with your plugin
 * and make a task by extending the PluginTask class and Override the abstract methods.
 * @author backspace119
 *
 */
public class TaskManager {

	private Plugin plugin;
	public TaskManager(Plugin plugin)
	{
		this.plugin = plugin;
	}
	Map<PluginTask, Integer> idCache = new HashMap<>();
	public int getTaskId(PluginTask task)
	{
		return idCache.get(task);
	}
	public void cancelTask(PluginTask task)
	{
		plugin.getServer().getScheduler().cancelTask(idCache.get(task));
		idCache.remove(task);
	}
	/**
	 * Runs a task that will delay the amount of secondsBeforeStart before starting a task that will repeat execution
	 * every amount of time from the secondsBetweenCycles
	 * @param task
	 * @param secondsBeforeStart
	 * @param secondsBetweenCycles
	 */
	public void scheduleRepeatingTask(PluginTask task, double secondsBeforeStart, double secondsBetweenCycles)
	{
		double bt = 20 * secondsBeforeStart;
		double ct = 20 * secondsBetweenCycles;
		BukkitTask bkt = plugin.getServer().getScheduler().runTaskTimer(plugin,task,(long) bt,(long) ct);
		idCache.put(task, bkt.getTaskId());
	}
	/**
	 * starts a task after the amount of input time.
	 * @param task
	 * @param secondsBeforeStart
	 */
	public void scheduleDelayedTask(PluginTask task, double secondsBeforeStart)
	{
		double t = 20 * secondsBeforeStart;
		BukkitTask bt = plugin.getServer().getScheduler().runTaskLater(plugin,task, (long) t);
		idCache.put(task, bt.getTaskId());
	}
	/**
	 * starts a task immediately
	 * @param task
	 */
	public void runTask(PluginTask task)
	{
		BukkitTask bt = plugin.getServer().getScheduler().runTask(plugin,task);
		idCache.put(task, bt.getTaskId());
	}
}
