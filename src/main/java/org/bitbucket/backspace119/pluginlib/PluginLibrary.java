/*
 *   Copyright (c) 2014 backspace119
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.bitbucket.backspace119.pluginlib;

import org.bukkit.plugin.java.JavaPlugin;
/**
 * the main class for the plugin version of this library.
 * 
 * You may either utilize this library as a plugin, a maven dependency, or some other form
 * of compiled package included with your code. Remember that if you use this in plugin form,
 * you must list it as a dependency in your plugin.yml and assert on your bukkit page that
 * server owners will have to have the correct version of this installed
 * @author backspace119
 *
 */
public class PluginLibrary extends JavaPlugin{

	@Override
	public void onEnable()
	{
		//pertinent information will be printed on startup
	}
}
