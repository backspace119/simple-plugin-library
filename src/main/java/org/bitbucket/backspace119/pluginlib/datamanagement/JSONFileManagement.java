/*
 *   Copyright (c) 2014 backspace119
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.bitbucket.backspace119.pluginlib.datamanagement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.plugin.Plugin;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Currently used to save HashMaps on a per file basis
 * makes for easy reloading of HashMaps and a clean, concise way of saving them
 * 
 * this will handle saving most any kind of data type later
 * @author backspace119
 *
 */
public class JSONFileManagement {

	final File file;
	JSONObject jsonMap;
	final File dirFile;
	public JSONFileManagement(String fileName, Plugin plugin)
	{
		dirFile = new File(plugin.getDataFolder() + "/jsonData");
		dirFile.mkdirs();
		fileName += ".json";
		file = new File(plugin.getDataFolder() + "/jsonData", fileName);
		
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * ONE HASHMAP PER FILE MANAGER! Only call this method once whenever you want
	 * to save a SINGLE HASHMAP! this file will not store multiple hashmaps currently
	 * and if you call this with a different hashmap it will save over your first
	 * A way around this is to save your HashMaps into a central HashMap with key entries
	 * that you can recall to find your HashMaps, then you can save the central HashMap, and it
	 * will save all of the maps inside of it. Note that when recalling this from the readHashMap() method,
	 * you will need to extract each of your HashMaps from this central Map. The recommended way
	 * to save custom objects to JSON is to make a method to turn it into a JSON ready string
	 * (i.e. put all the values you need to make the object into a JSONObject and then get the
	 * string value from said object) and then have a static method to create these objects from
	 * JSON strings (i.e. pull in a string and parse it back to a JSONObject and pull the 
	 * values you need out of it) in the future, the library will help handle these objects
	 * Note: this will take any type of Map, HashMaps are just the most commonly used
	 * and saved by this method
	 */
	public void writeHashMap(Map<?,?> map)
	{
		String mapString = JSONValue.toJSONString(map);
		try {
			PrintWriter fos = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8));
			fos.write(mapString);
			fos.close();
			System.out.println("file successfully written");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Use this on a file to reload a Map from it, the Map must be cast back to the
	 * type of map it was, if it is miscast you will have problems getting it re-instantiated.
	 * @return the map read from the file, if it could not be read, a new Map of generic types.
	 */
	public Map<?,?> readHashMap()
	{
		jsonMap = new JSONObject();
		try {
			if(file.exists()){
			
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
			//we want to quietly handle this, whilst 
			//handling the other exception here
			//in normal console
			String mapString = br.readLine();
			
			br.close();
			if(mapString != null)
			{
			JSONParser parser = new JSONParser();
			jsonMap = (JSONObject) parser.parse(mapString);
			}
			}
		} catch (IOException e) {
			//quietly handling failing of loading the map
			System.err.println(e.getStackTrace());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		
		return jsonMap;
		
		
		 
	}
	/**
	 * This will read JSON format Strings created by locationToJSON() and give you the
	 * exact Location object that was saved
	 * @param json the JSON to decode and reform the Location with
	 * @param plugin a refrence to your plugin so the World may be found
	 * @return the exact Location object that was saved
	 */
	public static Location jsonToLocation(String json, Plugin plugin)
	{
		JSONObject map = new JSONObject();
		JSONParser parser = new JSONParser();
		try{
			map = (JSONObject) parser.parse(json);
		}catch(ParseException e)
		{
			e.printStackTrace();
		}
		String world = (String)map.get("world");
		int x = Integer.valueOf((String)map.get("x"));
		int y = Integer.valueOf((String)map.get("y"));
		int z = Integer.valueOf((String)map.get("z"));
		float pitch = Float.valueOf((String)map.get("pitch"));
		float yaw = Float.valueOf((String)map.get("yaw"));
		return new Location(plugin.getServer().getWorld(world),x,y,z,yaw,pitch);
	}
	/**
	 * Use this to convert a Location into a single, nice, JSON string that you can store 
	 * and recall later to reconstruct the exact Location object from jsonToLocation()
	 * @param l the Location to convert to a JSON String
	 * @return the JSON format String of this object
	 */
	@SuppressWarnings("unchecked")
	public static String locationToJSON(Location l)
	{
		JSONObject map = new JSONObject();
		map.put("x", String.valueOf(l.getBlockX()));
		map.put("y", String.valueOf(l.getBlockY()));
		map.put("z", String.valueOf(l.getBlockZ()));
		map.put("world", l.getWorld().getName());
		
		map.put("pitch", String.valueOf((int)l.getPitch()));
		map.put("yaw", String.valueOf((int)l.getYaw()));
		return map.toJSONString();
	}
	
}
