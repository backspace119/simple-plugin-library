package org.bitbucket.backspace119.pluginlib.datamanagement;

import org.bukkit.configuration.file.FileConfiguration;

public interface IConfigHandler {

	FileConfiguration getConfig();
	void saveConfig();
	void reloadConfig();
}
