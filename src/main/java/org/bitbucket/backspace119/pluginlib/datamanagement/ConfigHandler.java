/*
 * 
 * @author backspace119
 *
 *
 *Copyright (C) 2019  backspace119
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *
 */
package org.bitbucket.backspace119.pluginlib.datamanagement;

import java.io.*;
import java.util.Objects;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

/**
 * 
 * Generic YAML config handler that I did not fully create. I've modified it to
 * keep with the bukkit api (i.e. staying out of deprecation)
 * 
 */
public class ConfigHandler implements IConfigHandler{

	private final String fileName;
	private FileConfiguration fileConfiguration = null;
	private File configFile = null;
	private final Plugin plugin;
	private final boolean saveToDisk;
	
	public ConfigHandler(Plugin plugin, String fileName, boolean saveToDisk) {
		if (plugin == null) throw new IllegalArgumentException("plugin cannot be null");
		//if (!plugin.isInitialized()) throw new IllegalArgumentException("plugin must be initiaized");
		this.plugin = plugin;
		this.fileName = fileName;
		this.configFile = new File(plugin.getDataFolder(), fileName);
		this.saveToDisk = saveToDisk;
	}

	@Override
	public void reloadConfig() {
		fileConfiguration = YamlConfiguration.loadConfiguration(configFile);
		BufferedReader defConfigStream = new BufferedReader(new InputStreamReader(Objects.requireNonNull(plugin.getResource(fileName))));
		YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
		fileConfiguration.setDefaults(defConfig);
		if (!configFile.exists()) {
			saveDefaultConfig();
			reloadConfig();
		}
	}

	@Override
	public FileConfiguration getConfig() {

		if (fileConfiguration == null) {
			this.reloadConfig();
		}
		return fileConfiguration;
	}

	@Override
	public void saveConfig() {
		if (saveToDisk) {
			if (!(fileConfiguration == null || configFile == null)) {
				try {
					getConfig().save(configFile);
				} catch (IOException ex) {
					plugin.getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
				}
			}
		}
	}

	public void saveDefaultConfig() {
		if (saveToDisk) {
			plugin.saveResource(fileName, true);
		}
	}

}
